﻿using API.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Data
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options):
            base(options)
        {

        }
        public DbSet<News> News { get; set; }
        public DbSet <Notice> Notice { get; set; }
    }
}
