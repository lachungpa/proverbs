﻿using API.Data;
using API.DTOs;
using API.Entity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class NewsController: BaseApiController
    {
        private readonly DataContext _data;
        public NewsController(DataContext data)
        {
            _data = data;
        }
        [HttpGet("Display")]
        public ActionResult<List<News>> GetNews()
        {
            return _data.News.ToList();            
        }

        [HttpGet("{id}")]
        public ActionResult<News> GetByID(int id)
        {
            return _data.News.Find(id);
        }

        [HttpPost]
        public ActionResult<News> AddNews(NewsDto news)
        {
            _data.News.Add(new News 
            {
                Body=news.Body,
                Reference=news.Reference,
                Title=news.Title
            });


            _data.SaveChanges();

            return Ok(news);
        }

    }
}
